import java.util.Random;

public class RouletteWheel {
    private Random randomNumber;
    private int lastSpin;

    public RouletteWheel(){
        this.randomNumber = new Random();
        this.lastSpin = 0;
    }

    public void spin(){
        this.lastSpin =  randomNumber.nextInt(37);
    }

    public int getlastSpin(){
        return this.lastSpin;
    }


}
